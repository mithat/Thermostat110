/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2016 Mithat Konar
 * @brief     Main Arduino file for a thermostatic fan control system.
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 *
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

// ========================
// Includes
// ========================
#include <Arduino.h>
#include "LiquidCrystal.h"
#include "BigCrystal.h"
#include "Display.h"
#include "types.h"
#include "config.h"
#include "PushButtonLatching.h"
#include "AsyncTimer.h"
#include "Adafruit_MCP9808.h"

// ========================
// Function prototypes
// ========================
// Button events:
void desiredTempUp();
void desiredTempDown();
void changeMode();

// Timer actions:
void readTempSensor();
void backlightOn();
void backlightOff();

// Utils
void setFanPower(bool);
void setActualTemp(float);

// ========================
// Constants
// ========================

const unsigned long TEMP_CHECK_PERIOD = 1000; // msec period for checking temp
const unsigned long BACKLIGHT_TIMEOUT = 2000; // msec for backlight to turn off
const float INITIAL_DESIRED_TEMP = (74 - 32) * 5.0 / 9.0; // inital thermostat setting
const float TEMP_DIFF_DELTA = 0.01; // degrees C considered a sensor change
const float TEMP_HYSTERESIS = 0.1; // turn on/off fan only if difference exceeds this
const float C_INCR = 5.0 / 9.0; // the amount to increment C temp to get 1 degree F

// ========================
// Globals
// ========================

struct Globals
{
    // State variables
    Mode mode = Mode::OFF; // heating, cooling, on, off
    float actualTemp = 0.0; // the measured temperature in degrees C
    float desiredTemp = 0.0; // the "thermostat setting" in degrees C
    bool isFan = true;

    // Sensors
    Adafruit_MCP9808 tempsensor = Adafruit_MCP9808();

    // UI
    Display * const display =
            new Display(new BigCrystal(new LiquidCrystal(LCD_RS_PIN,
                                                         LCD_ENABLE_PIN,
                                                         LCD_D4_PIN,
                                                         LCD_D5_PIN,
                                                         LCD_D6_PIN,
                                                         LCD_D7_PIN)));

    // Controls
    PushButtonLatching * const desiredTempUpButton = new PushButtonLatching(BTN_TEMP_UP_PIN, desiredTempUp);
    PushButtonLatching * const desiredTempDownButton = new PushButtonLatching(BTN_TEMP_DOWN_PIN, desiredTempDown);
    PushButtonLatching * const changeModeButton = new PushButtonLatching(BTN_MODE_PIN, changeMode);

    // Timers
    AsyncTimer * const tempsenseTimer = new AsyncTimer(TEMP_CHECK_PERIOD, readTempSensor);
    AsyncTimer * const backlightTimer = new AsyncTimer(BACKLIGHT_TIMEOUT, backlightOff, backlightOn);
} g;

// ====================
// Setup
// ====================

void setup()
{
    debugEnableSerial(19200);
    dprintln(F("-----------"));
    dprintln(F("Begin"));
    dprintln(F("-----------"));

    pinMode(BTN_MODE_PIN, INPUT_PULLUP);
    pinMode(BTN_TEMP_UP_PIN, INPUT_PULLUP);
    pinMode(BTN_TEMP_DOWN_PIN, INPUT_PULLUP);
    pinMode(LCD_BACKLIGHT_PIN, OUTPUT);
    pinMode(FAN_CONTROL_PIN, OUTPUT);

    backlightOff();

    // Init temperature sensor:
    while (!g.tempsensor.begin())
    {
        delay(100);
    }

    delay(100);
    readTempSensor();

    // initial desiredTemp is the actualTemp on startup:
    g.desiredTemp = INITIAL_DESIRED_TEMP;

    // init display
    g.display->updateDesiredTempF(g.desiredTemp);
    g.display->updateMode(g.mode);

    setFanPower(g.isFan);
}

// ====================
// Loop
// ====================

void loop()
{
    // Handle user events
    g.desiredTempUpButton->poll();
    g.desiredTempDownButton->poll();
    g.changeModeButton->poll();

    // Handle system events

    // Housekeeping
    g.tempsenseTimer->checkExpiration();
    g.backlightTimer->checkExpiration();
}

// ====================
// Events/actions
// ====================

/**
 * Increment the desired temperature by one degree F.
 */
void desiredTempUp()
{
    // turn on backlight
    g.backlightTimer->start();

    g.desiredTemp += C_INCR;
    g.display->updateDesiredTempF(g.desiredTemp);

    // Turn fan on/off as needed:
    setActualTemp(g.actualTemp);
}

/**
 * Decrement the desired temperature by one degree F.
 */
void desiredTempDown()
{
    // turn on backlight
    g.backlightTimer->start();

    g.desiredTemp -= C_INCR;
    g.display->updateDesiredTempF(g.desiredTemp);

    // Turn fan on/off as needed:
    setActualTemp(g.actualTemp);
}

/**
 * Step through different thermostat modes.
 */
void changeMode()
{
    // turn on backlight
    g.backlightTimer->start();

    switch (g.mode)
    {
    case Mode::HEATING:
        g.mode = Mode::COOLING;
        break;
    case Mode::COOLING:
        g.mode = Mode::ON;
        break;
    case Mode::ON:
        g.mode = Mode::OFF;
        break;
    case Mode::OFF:
        g.mode = Mode::HEATING;
        break;
    }
    g.display->updateMode(g.mode);

    // Turn fan on/off as needed:
    setActualTemp(g.actualTemp);
}

/**
 * Timer action for reading the temperature sensor.
 *
 * Read the sensor and take action if there is a temperature change. Restart
 * timer.
 */
void readTempSensor()
{
    float newTemp = g.tempsensor.readTempC();
    float diff = newTemp - g.actualTemp; // poor person's fabs()
    diff = (diff > 0) ? diff : 0 - diff;

    if (diff >= TEMP_DIFF_DELTA)
    {
        setActualTemp(newTemp);
    }

    g.tempsenseTimer->start();
}

/**
 * Timer action for turning backlight on.
 */
void backlightOn()
{
    g.display->setBacklight(true);
}

/**
 * Timer action for turning backlight off.
 */
void backlightOff()
{
    g.display->setBacklight(false);
}

// ====================
// Utils
// ====================

/**
 * Set the state of the system to reflect the specified temperature.
 *
 * @param tempC  The actual ambient temperature (as reported by a sensor).
 */
void setActualTemp(float tempC)
{
    g.actualTemp = tempC;
    g.display->updateActualTempF(g.actualTemp);

    switch (g.mode)
    {
    case Mode::HEATING:
        if (!g.isFan && (g.actualTemp < (g.desiredTemp - TEMP_HYSTERESIS)))
        {
            setFanPower(true);
        }
        else if (g.actualTemp > (g.desiredTemp + TEMP_HYSTERESIS))
        {
            setFanPower(false);
        }
        break;
    case Mode::COOLING:
        if (!g.isFan && (g.actualTemp > (g.desiredTemp + TEMP_HYSTERESIS)))
        {
            setFanPower(true);
        }
        else if (g.actualTemp < (g.desiredTemp - TEMP_HYSTERESIS))
        {
            setFanPower(false);
        }
        break;
    case Mode::ON:
        if (!g.isFan)
        {
            setFanPower(true);
        }
        break;
    case Mode::OFF:
        if (g.isFan)
        {
            setFanPower(false);
        }
        break;
    }
}

/**
 * Unconditionally turn the fan on and off.
 *
 * @param isOn  Whether the fan should be on off.
 */
void setFanPower(bool isOn)
{
    if (g.isFan != isOn)
    {
        g.isFan = isOn;
        dprint(isOn);
        dprint(' ');
        dprint(g.actualTemp);
        dprint(' ');
        dprintln(g.desiredTemp);
    }

    // activate fan pin
    digitalWrite(FAN_CONTROL_PIN, isOn);

    // set display
    g.display->updateFan(isOn);



    // TODO: make fan on/off work on a timer that changes state every 2 sec?
}
