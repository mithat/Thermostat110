/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2015-2016 Mithat Konar
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UI_PUSHBUTTONLATCHING_H
#define UI_PUSHBUTTONLATCHING_H

#include "PushButton.h"

/**
 * Encapsulation of a momentary HW switch that fires one action when pressed and another when released.
 * 
 * A PushButtonLatching triggers an action the first time it is active when
 * being polled and an unlatch action the first time it not active while being 
 * polled. Member functions action and unlatchAction do not need to manage
 * the latch state; they need only specify what happens when the switch is 
 * first pressed and then what happens when it is released. The unlatch action 
 * is optional, in which case the switch acts as a one-shot.
 */
class PushButtonLatching : public PushButton
{
public:
    PushButtonLatching(uint8_t, PushButtonActionFuncPtr, PushButtonActionFuncPtr = NULL, unsigned long = PushButton::kDefaultDebounce, uint8_t = PushButton::kDefaultActiveState);
    virtual bool poll();

protected:
    PushButtonActionFuncPtr m_unlatchAction;   // The action associated with this switch.
    bool m_isLatched;

private:
};

#endif // UI_PUSHBUTTONLATCHING_H
