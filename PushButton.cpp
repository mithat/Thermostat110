/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2015-2016 Mithat Konar
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PushButton.h"

namespace ui
{

/**
 * Constructor for pushbutton switches.
 * 
 * The inPin will be set as an INPUT_PULLUP if activeState is LOW and
 * as in INPUT if activeState is HIGH.
 *
 * @param inPin         The pin associated with this switch.
 * @param action        The action triggered by this switch.
 * @param debounceLen   msecs needed to debounce switch (defaults to PushButton::DEFAULT_DEBOUNCE).
 * @param activeState   HIGH if switch is active high, LOW otherwise (defaults to DEFAULT_ACTIVE_STATE).
 */
PushButton::PushButton(uint8_t inPin, PushButtonActionFuncPtr action, unsigned long debounceLen, uint8_t activeState)
{
    m_inPin = inPin;
    m_action = *action;
    m_activeState = activeState;
    m_debounceLen = debounceLen;
    
    pinMode(m_inPin, (activeState == LOW) ? INPUT_PULLUP : INPUT);
}

/**
 * Poll the switch and take action if needed.
 *
 * @return true iff action was taken.
 */
bool PushButton::poll()
{
    if (digitalRead(m_inPin) == m_activeState)
    {
        delay(m_debounceLen);
        if (digitalRead(m_inPin) == m_activeState)
        {
            (*m_action)();
            return true;
        }
    }
    return false;
}

}
