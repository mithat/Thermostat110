# This file is part of Thermostat110.

# Project config
ARDUINO_LIBS = BigCrystal LiquidCrystal AsyncTimer Adafruit_MCP9808_Library Wire
BOARD_TAG = nano
BOARD_SUB=atmega328
MONITOR_PORT = /dev/ttyUSB0
MONITOR_CMD = screen-wrap

# "Platform" config
ARDUINO_QUIET = true
ARDUINO_SKETCHBOOK = $(HOME)/Arduino
ARDUINO_DIR = $(HOME)/opt/arduino
ARDMK_DIR = $(HOME)/Build/Arduino-Makefile
#~ AVR_TOOLS_DIR = /usr

# Include parent Makefile from <https://github.com/sudar/Arduino-Makefile>
include $(HOME)/Build/Arduino-Makefile/Arduino.mk
