/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2016 Mithat Konar
 * @brief     Pinout and config parameters for Thermostat110.
 * @section   LICENSE
 * 
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CONFIG_H
#define CONFIG_H

#undef  DEBUG
//#define DEBUG

#undef DEBUG_SERIAL
#define DEBUG_SERIAL

// TODO: FIX dprintln() and ddprintebug() not accepting non-string arguments.
#ifdef DEBUG_SERIAL
#define debugEnableSerial(x) (Serial.begin(x))
#define dprintln(x) (Serial.println(x))
#define dprint(x) (Serial.print(x))
#else
#define debugEnableSerial(x)
#define dprintln(x)
#define dprint(x)
#endif

// ========================
// I/O
// ========================

const uint8_t LCD_DIM_LEVEL = 36;

// Display
/*
   The LCD circuit:
 * LCD RS pin to digital pin 12
 * LCD Enable pin to digital pin 11
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * 10K resistor:
 * ends to +5V and ground
 * wiper to LCD VO pin (pin 3)
 */

const uint8_t LCD_RS_PIN = 12;
const uint8_t LCD_ENABLE_PIN = 11;
const uint8_t LCD_D4_PIN = 5;
const uint8_t LCD_D5_PIN = 4;
const uint8_t LCD_D6_PIN = 3;
const uint8_t LCD_D7_PIN = 2;
const uint8_t LCD_BACKLIGHT_PIN = 10;  // PWM

// I2C/Wire
// no need to configure the following:
// A4 (SDA)
// A5 (SCL)

// Buttons (inputs)
const uint8_t BTN_TEMP_UP_PIN = 8;
const uint8_t BTN_TEMP_DOWN_PIN = 7;
//const uint8_t BTN_MODE_PIN = 6;
#define BTN_MODE_PIN A2

// Fan control
const uint8_t FAN_CONTROL_PIN = 9;
 

#endif /* CONFIG_H */
