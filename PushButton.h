/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2015-2016 Mithat Konar
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UI_PUSHBUTTON_H
#define UI_PUSHBUTTON_H

#include <limits.h>
#include <Arduino.h>

/**
 * Encapsulation of a HW switch that triggers an action as long as it is pressed.
 * 
 * PushButton debounces the HW switch in software.
 */
class PushButton
{
public:
    static const unsigned long kDefaultDebounce = 20;
    static const uint8_t kDefaultActiveState = LOW;

    typedef void (*PushButtonActionFuncPtr)();
    PushButton(uint8_t, PushButtonActionFuncPtr, unsigned long = PushButton::kDefaultDebounce, uint8_t = PushButton::kDefaultActiveState);
    virtual bool poll();
    // Accessors/mutators are YAGNI.

protected:
    uint8_t m_inPin; // Pin number associated with this switch.
    unsigned long m_debounceLen; // msecs needed to debounce switch
    uint8_t m_activeState; // HIGH iff active high, LOW otherwise.
    PushButtonActionFuncPtr m_action; // The action associated with this switch.

private:
};

#endif // UI_PUSHBUTTON_H
