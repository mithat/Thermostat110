/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2016 Mithat Konar
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Display.h"
#include "config.h"

/**
 * Initialize the Display with the given lcd.
 * @param lcd  The BigCrystal lcd associated with this Display.
 */
Display::Display(BigCrystal* lcd)
{
    m_lcd = lcd;
    m_lcd->begin(16, 2);
    m_tempF = 0;
}

/**
 *  Turn the backlight on or off.
 */
void Display::setBacklight(bool isOn)
{
    // TODO: use AsyncTimer to fade in and out?
    if (isOn) digitalWrite(LCD_BACKLIGHT_PIN, HIGH);
    else analogWrite(LCD_BACKLIGHT_PIN, LCD_DIM_LEVEL);
}

/**
 * Display the actual temperature in degrees F.
 * 
 * @param actualTemp  Actual temperature in degrees C.
 */
void Display::updateActualTempF(float actualTemp)
{
#ifndef DEBUG
    const int BUFLEN = 5; // nominally 2-char, but 3-char needed for >99
    char buf[BUFLEN];

    int tempF = c_to_f_int(actualTemp);

    if (tempF != m_tempF)
    {
        m_tempF = tempF; // cache new value

        // blank out whatever was there before
        m_lcd->setCursor(5, 0);
        m_lcd->print("        ");
        m_lcd->setCursor(5, 1);
        m_lcd->print("       ");

        if (-10 < tempF && tempF < 100) // if nums fit into 2 chars ...
        {
            // format right justified in two char field
            snprintf(buf, BUFLEN - 1, "%2d", tempF);
            m_lcd->printBig(buf, 5, 0);

            // degrees symbol
            m_lcd->setCursor(13, 0);
            m_lcd->print((char) 223);
        }
        else
        {
            // format right justified in three char field with degrees marker
            snprintf(buf, BUFLEN, "%3d%c", tempF, (char) 223);
            // render with small text
            m_lcd->setCursor(7, 1);
            m_lcd->print(buf);
        }
    }
#else
    // Development display:
    float tempF = c_to_f(actualTemp);
    m_lcd->setCursor(5, 0);
    m_lcd->print(tempF);
        
    int tempF_i = c_to_f_int(actualTemp);

    m_lcd->setCursor(5, 1);
    m_lcd->print(tempF_i);
#endif
}

/**
 * Display the desired (i.e., thermostat setting) temperature in degrees F.
 * 
 * @param desiredTemp Desired temperature in degrees C.
 */
void Display::updateDesiredTempF(float desiredTemp)
{
    int tempF = c_to_f_int(desiredTemp);
    const int BUFLEN = 5;
    char buf[BUFLEN]; // temp var

    // format right justified w/ or w/o degree symbol in four char field
    // snprintf(buf, BUFLEN, "%3d%c", tempF, (char)223);
    snprintf(buf, BUFLEN, "%4d", tempF);

    m_lcd->setCursor(12, 1);
    m_lcd->print(buf);
}

/**
 * Display the current mode.
 * @param mode  The mode to display.
 */
void Display::updateMode(Mode mode)
{
    const int BUFFLEN = 5;
    char *strs[] = {"heat", "cool", "on", "off"};
    uint8_t strnum;
    char buf[BUFFLEN];

    switch (mode)
    {
    case Mode::HEATING: strnum = 0;
        break;
    case Mode::COOLING: strnum = 1;
        break;
    case Mode::ON: strnum = 2;
        break;
    case Mode::OFF: strnum = 3;
        break;

    }

    snprintf(buf, BUFFLEN, "%-4s", strs[strnum]);

    m_lcd->setCursor(0, 1);
    m_lcd->print(buf);
}

/**
 * Display whether the fan is on or not.
 * @param isFan  Whether the fan is on or not.
 */
void Display::updateFan(bool isFan)
{
    char bufchar = isFan ? (char) 235 : ' ';

    m_lcd->setCursor(0, 0);
    m_lcd->print(bufchar);
}

/**
 * Convert degrees C to degrees F.
 * 
 * @param tempC The temperature in degrees C.
 * @return tempC's F equivalent rounded to an int.
 */
int Display::c_to_f_int(float tempC)
{
    float tempF = (tempC * 9.0 / 5.0) + 32.0;
    return static_cast<int> (floor(tempF + 0.5));
}

/**
 * Convert degrees C to degrees F.
 * 
 * @param tempC The temperature in degrees C.
 * @return tempC's F equivalent.
 */
float Display::c_to_f(float tempC)
{
    float tempF = (tempC * 9.0 / 5.0) + 32.0;
    return tempF;
}