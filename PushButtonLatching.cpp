/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2015-2016 Mithat Konar
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PushButtonLatching.h"

//#include "config.h"

namespace ui
{

/**
 * Constructor for a one shot pushbutton switch.
 *
 * @param inPin         The pin associated with this switch.
 * @param action        The latching action triggered by this switch.
 * @param unlatchAction The unlatching action triggered by this switch.
 * @param debounceLen   msecs needed to debounce switch (defaults to PushButton::DEFAULT_DEBOUNCE).
 * @param activeState   HIGH if switch is active high, LOW otherwise (defaults to DEFAULT_ACTIVE_STATE).
 */
PushButtonLatching::PushButtonLatching(uint8_t inPin, PushButtonActionFuncPtr action, PushButtonActionFuncPtr unlatchAction, unsigned long debounceLen, uint8_t activeState)
: PushButton(inPin, action, debounceLen, activeState)
{
    m_unlatchAction = unlatchAction;
    m_isLatched = false;
}

/** 
 * Poll the switch and take action if needed. Virtual.
 * 
 * @return true iff latching action was taken.
 */
bool PushButtonLatching::poll()
{
    if (digitalRead(m_inPin) == m_activeState) // catch first time going from inactive to active state (latching)
    {
        delay(m_debounceLen);
        if (digitalRead(m_inPin) == m_activeState)
        {
            if (!m_isLatched)   // first poll going active after being inactive (latch)
            {
                m_isLatched = true;
                (*m_action)();
                return true;
            }
        }
    }
    else if (m_isLatched)   // i.e., if (digitalRead(m_inPin) != m_activeState) && m_isLatched); catch first time going from active to inactive state unlatching)
    {
        delay(m_debounceLen);
        if (digitalRead(m_inPin) != m_activeState) // first poll going inactive after being active (unlatch)
        {
            m_isLatched = false;
            if (m_unlatchAction)
            {
                (*m_unlatchAction)();
            }
        }
    }

    return false;
}

}
