/**
 * @file
 * @author    Mithat Konar (mithat ~at~ mithatkonar.com)
 * @copyright Copyright (C) 2016 Mithat Konar
 * @section   LICENSE
 *
 * This file is part of Thermostat110.
 * 
 * Thermostat110 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thermostat110 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Thermostat110.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DISPLAY_H
#define DISPLAY_H

#include <BigCrystal.h>
#include "types.h"

/**
 * Encapsulate a 16x2 character LCD display for Thermostat110.
 */
class Display
{
public:
    Display(BigCrystal *lcd);
    void updateDesiredTempF(float desiredTemp);
    void updateActualTempF(float actualTemp);
    void updateMode(Mode mode);
    void updateFan(bool isFan);
    void setBacklight(bool isOn);

protected:
    BigCrystal *m_lcd; // system display
    int m_tempF; // for caching displayed value.
    // TODO: cache everything?
    int c_to_f_int(float);
    float c_to_f(float);
};

#endif /* DISPLAY_H */
